package com.harsha.DiIoc;

import java.util.Iterator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class App 
{
    public static void main( String[] args )
    {
    	Iterator it;
    	ApplicationContext context= new ClassPathXmlApplicationContext("Diioc.xml");
		System.out.println("Student 1 details:");
		Student student1=(Student)context.getBean("student1");
		System.out.println(student1.getStudentId());
		System.out.println(student1.getStudentName());
		it=student1.getStudentTest().iterator();
		while(it.hasNext()) {
			Test test=(Test)it.next();
			System.out.println(test.getTestId());
			System.out.println(test.getTestTitle());
			System.out.println(test.getTestMarks());
		}
		System.out.println("Student 2 details:");
		Student student2=(Student)context.getBean("student2");
		System.out.println(student2.getStudentId());
		System.out.println(student2.getStudentName());
		it=student2.getStudentTest().iterator();
		while(it.hasNext()) {
			Test test=(Test)it.next();
			System.out.println(test.getTestId());
			System.out.println(test.getTestTitle());
			System.out.println(test.getTestMarks());
		}
    }
}
